import access_tokens
import json
import requests
import telebot
from telebot import types
import datetime
import traceback
import time

bot = telebot.TeleBot(access_tokens.TELEGRAM_TOKEN)

def refresh_access_token():
    # response = requests.post("https://service.campus.rwth-aachen.de/oauth2/oauth2.svc/token",
    response = requests.post("https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/token",
            data={
                "client_id": access_tokens.CLIENT_ID,
                "refresh_token": access_tokens.REFRESH_TOKEN,
                "grant_type": "refresh_token"
            }
    )
    if response.status_code != 200:
        print(response.reason)
        return None
    data = json.loads(response.text)
    return data["access_token"]

def is_token_still_valid():
    response = requests.post("https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/tokeninfo",
            data={
                "client_id": access_tokens.CLIENT_ID,
                "access_token": access_token
            }
    )
    if response.status_code != 200:
        print(response.reason)
        return None
    data = json.loads(response.text)
    return data["state"] == "valid"


def ask_with_token(path, post_data):
    if not is_token_still_valid():
        global access_token
        access_token = refresh_access_token()
    post_data["token"] = access_token
    response = requests.get("https://moped.ecampus.rwth-aachen.de/proxy/api/v2/" + path, params=post_data)
    if response.status_code != 200:
        print(response.reason)
        return None
    data = json.loads(response.text)
    return data
    
access_token = refresh_access_token()

# Type emojis

emojis = {
    "pig": "🐷",
    "poultry": "🐔",
    "beef": "🐮",
    "fish": "🐟",
    "vegetarian": "🌱",
    "vegan": "🥑",
}

# verbosity is in {0, 1}
def parse_menu(response, verbosity, date="", weekday="", category="", contents=""):
    data = response["Data"]
    formatted_string = ""
    formatted_string += "**Plan für " + data["canteenName"] + "** "
    if category != "":
        formatted_string += "(Kategorie enthält " + category + ") "
    if contents != "":
        formatted_string += "\nNur Gerichte mit Label " + emojis[contents]
    formatted_string += "\n\n"
    for day in data["dayMenues"]:
        # Only include days that match weekday and date
        include_this = True
        for key, value in {"weekDay": weekday, "displayDate": date} \
            .items():
            if value != None and not value.lower() in day[key].lower():
                include_this = False
                break
        if not include_this:
            continue

        formatted_string += "_" + day["weekDay"] + ", " + day["displayDate"] + "_\n\n"

        # Menues sometimes have duplicates weirdly, we remove them here
        menues = [i for n, i in enumerate(day["menues"]) if i not in day["menues"][n + 1:]]
        
        for m in menues:
            if m["category"] == None:
                continue
            if category != None and not category.lower() in m["category"].lower():
                continue
            if m["mealTypes"] != None and contents != "" and not contents in m["mealTypes"]:
                continue
            formatted_string += "*" + m["category"] + "* "
            if "mealTypes" in m and len(m["mealTypes"]) > 0:
                for meal_type in m["mealTypes"]:
                    if meal_type in emojis:
                        formatted_string += emojis[meal_type]
                    else:
                        formatted_string += " " + meal_type + " "
            formatted_string += " " + m["price"] + "\n"

            formatted_string += m["menu"]
            formatted_string += "\n"
            
            if verbosity >= 1:
                formatted_string += "_Nährwert:_\n"
                for info in m["nutritionInfo"]:
                    formatted_string += info + ": " + str(m["nutritionInfo"][info]) + "\n"

            formatted_string += "\n"
    return formatted_string

# Mensa names
mensae = ask_with_token("Canteens/GetCanteens", {})
mensa_names = [it["canteenName"] for it in mensae["Data"]]
mensa_codes = {}
for mensa in mensae["Data"]:
    mensa_codes[mensa["canteenName"]] = mensa["canteenId"]

# Maps chat_ids to last executed command (heute, morgen, alles)
last_commands = {}
# Time frames (days) for commands
find_today = lambda: datetime.datetime.now().strftime("%d.%m.%Y")
find_tomorrow = lambda: (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%d.%m.%Y")
time_frames = {
        "/heute": find_today,
        "/morgen": find_tomorrow,
        "/alles": lambda: ""
}

@bot.message_handler(commands=['license'])
def license(message):
    bot.reply_to(message, "This bot is FOSS! https://git.rwth-aachen.de/h/mensa_bot")

@bot.message_handler(commands=['start', 'help'])
def help(message):
    help_string = "*Hallo!* Ich bin der Mensabot.\n\n" + \
        "Kommandos: /heute, /morgen, /alles, /tag [Montag - Freitag]\n" + \
        "Mensa: Direkt spezifizieren mit Abkürzung, zB aca für Academica\n" + \
        "Kategorie: Direkt spezifizieren mit Abkürzung, zB klass für Klassiker\n" + \
        "Inhalt: `only:(" + "|".join(emojis.keys()) + ")` \n" + \
        "Optionen: -n für Ernährungswerte\n\n" \
        "Beispiele:\n" \
        "/heute -n academica\n" \
        "/morgen ahorn klassiker\n" \
        "/alles vita only:vegan\n" \
        "/tag mittwoch bistro\n" \
        "/morgen"
    bot.reply_to(message, help_string, parse_mode="markdown")

# Get a list of mensae
@bot.message_handler(commands=['heute', 'morgen', 'alles', 'tag'])
def choose_mensa(message):
    last_commands[message.chat.id] = message.text
    try:
        execute(message, None, message.text)
    except Exception as e:
        print(e)

# Get info from the selection
@bot.message_handler(regexp="(" + "|".join(mensa_names) + ")")
def simple_info(message):   
    last_command = "/heute"
    if message.chat.id in last_commands:
        last_command = last_commands[message.chat.id]
    try:
        execute(message, message.text, last_command)
    except Exception as e:
        print(e)
    
def execute(message, mensa_name, last_command):
    args = last_command.split(" ")
    additional_args = args[1:]

    time_frame = ""
    weekday = ""
    if args[0].split("@")[0] in time_frames:
        time_frame = time_frames[args[0].split("@")[0]]()
    elif args[0] == "/tag":
        if len(additional_args) == 0:
            return
        weekday = additional_args[0]
        additional_args.remove(weekday)

    verbosity = 0
    if "-n" in additional_args:
        additional_args.remove("-n")
        verbosity = 1

    found_mensa = mensa_name
    
    if mensa_name == None:
        for mensa_n in additional_args:
            for name in mensa_names:
                if mensa_n.lower() in name.lower():
                    mensa_name = name
                    additional_args.remove(mensa_n)
                    break
    
    # Look for contents (beef, vegan etc.) in arguments
    contents = ""
    if len(additional_args) > 0:
        for arg in additional_args:
            if not arg.startswith("only:"):
                continue
            arg_cat = arg[len("only:"):]
            for cat in emojis.keys():
                if arg_cat.lower() in cat.lower():
                    contents = cat
                    additional_args.remove(arg)
                    break


    possible_category = ""
    # Maybe there is a category in there (we know that it's not a day)
    if len(additional_args) > 0:
        possible_category = additional_args[0]

    # If we don't have a mensa name, supply one first
    if mensa_name == None:
        # Get current list of all cantines
        # Make a nice reply keyboard
        markup = types.ReplyKeyboardMarkup()
        for n in mensa_names:
            markup.add(types.KeyboardButton(n))
        bot.reply_to(message, "Bitte Mensa auswählen (oder nächstes mal in deine Nachricht" +
                " schreiben :))", reply_markup=markup)
        return

    
    canteen_id = mensa_codes[mensa_name]
    response = ask_with_token("Canteens/GetWeekMenu", {"canteenId": canteen_id})

    rich_text = parse_menu(response, verbosity, weekday=weekday, date=time_frame,
            category=possible_category, contents=contents)

    markup = types.ReplyKeyboardRemove(selective=False)

    lines = rich_text.split("\n")
    messages = []
    m = ""
    for l in lines:
        if len(m + l) < 4096:
            m += l + "\n"
        else:
            messages.append(m)
            m = ""
    messages.append(m)

    if len(messages) > 3:
        bot.reply_to(message, "Das ist leider zu lang. Sei genauer.",
                reply_markup=markup, parse_mode="markdown")
        return

    for substring in messages:
        bot.reply_to(message, substring, reply_markup=markup, parse_mode="markdown")

# https://github.com/eternnoir/pyTelegramBotAPI/issues/206
def telegram_polling():
    while True:
        try:
            bot.polling(none_stop=True)
        except:
            traceback_error_string=traceback.format_exc()
            with open("error.log", "a") as myfile:
                myfile.write("\r\n\r\n" + time.strftime("%c")+"\r\n<<ERROR polling>>\r\n"+ traceback_error_string + "\r\n<<ERROR polling>>")
            bot.stop_polling()
            time.sleep(10)

if __name__ == '__main__':    
    telegram_polling()
